# Projet Training Center :

## Structure du projet :

- Un dossier Diagrammes qui contient :
    
  - Un diagramme de Classes qui représente les tables de la DB.
  - Un diagramme Utilisateur qui relie chaque les utilsateurs potentiels à
  des fonctionnalités.

- Un dossier Maquette interface qui contient quelques maquettes pour l'interface.
- Un Dossier SQL qui contient un fichier .sql pour générer toutes les tables de
de la base de données avec toutes ses relations et un petit jeu de données.


## Utilisateurs :

- Eleves
- Formtateurs
- Secrétariat
- Admin

## Fonctinnalités :

- Dépôt/Consultation/Suppression de devoirs par promotion.
- Dépôt/Consultation/Suppression de commentaires sur les devoirs.
- Demande de document administratif à des élèves par le secrétariat.
- Commentaires sur une demande de document.
- Dépôt/Consultationde/Suppression de News.
- Dépôt/Consultation/Suppression de commentaires.
- Création/Consultation/Suppression d'élèves et de promotions.


  ![image](Diagrammes/Diagramme_utilisateur.png)

## Tables SQL :


- Une table user_documents liée à une table users en One to Many.
- Une table users liée à une table promotion en One to Many.
- Une table news_comments liée à une table new en One to Many.
- Une table homework_remarks liée à homework en One to Many.
- Une table document_query_remarks liée à document_querys. 


  ![image](Diagrammes/Diagramme_de_classes.jpg)

## Les entités :

- Une entité a été crée pour chaque table contentant des 
accesseurs get/set pour chaque propriété.
- Elles continnent également chacunes trois constructeurs : Un premier
constructeur vide, un second constructeur sans Id et un troisième 
comprenant toutes le propriétés.

## Les Repositories et leurs Méthodes :

- Chaque entité est liée à un Repository qui contient des méthodes qui se 
connectent à la base de données.

Les repos contiennent :

- Une méthode save() qui prend en argument un objet de l'entité qui lui
correspond et le transforme en enregistrement dans la base de données.
- Une méthode findById() qui prend en argument un Id et instancie un objet
de l'entité en fonction des éléments contenu dans la ligne visée.
- Pour récupérer une liste d'objets contenus dans une certaine table
selon une clé étrangère correspondante on utilise findByAnotherElementId().
- Une méthode deleteById() qui prend en argument un Id et supprime l'enregistremnt
correspondant dans la table visée.

## Remarques et Anomalies :

- La méthode setBirthDate() de l'entité User ainsi que ses constructeurs, transforment automatiquement 
une String en LocalDate. Nous n'avons pas eu le temps d'étendre cette technique aux autres entités.
- Certaines méthodes save() et delete() des repos retournent des booléens, tandis que d'autres
ne le font pas. Cela dépend de qui a fait ces méthodes.
- Les entités et repos de News et NewsComment ont été modifiés afin de faire en sorte que
les instances de l'entité News venues de la base de données contiennent dans 
leurs propriétés une liste des NewsComment qui leur correspondent.

# !!! Manque de tests :

Nous n'avons pas eu le temps de tester les Repos :

- PromotionRepository
- DocumentQueryRepository
- DocumentQueryCommentRepository










