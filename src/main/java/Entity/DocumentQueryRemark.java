package Entity;

import java.time.LocalDate;

public class DocumentQueryRemark {

  private Integer id;
  private Integer documentQueryId;
  private String author;
  private String authorStatus;
  private String content;
  private LocalDate creationDate;

  public DocumentQueryRemark() {
  }

  public DocumentQueryRemark(Integer documentQueryId, String author, String authorStatus,
      String content, LocalDate creationDate) {
    this.documentQueryId = documentQueryId;
    this.author = author;
    this.authorStatus = authorStatus;
    this.content = content;
    this.creationDate = creationDate;
  }

  public DocumentQueryRemark(Integer id, Integer documentQueryId, String author,
      String authorStatus, String content, LocalDate creationDate) {
    this.id = id;
    this.documentQueryId = documentQueryId;
    this.author = author;
    this.authorStatus = authorStatus;
    this.content = content;
    this.creationDate = creationDate;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getDocumentQueryId() {
    return documentQueryId;
  }

  public void setDocumentQueryId(Integer documentQueryId) {
    this.documentQueryId = documentQueryId;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getAuthorStatus() {
    return authorStatus;
  }

  public void setAuthorStatus(String authorStatus) {
    this.authorStatus = authorStatus;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public LocalDate getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDate creationDate) {
    this.creationDate = creationDate;
  }
}
