package Entity;

import java.time.LocalDate;

public class DocumentQuery {

  private Integer id;
  private Integer idUser;
  private String demand;
  private String document;
  private String demandStatus;
  private LocalDate demandDate;
  private LocalDate demandDeadline;

  public DocumentQuery() {
  }

  public DocumentQuery(Integer idUser, String demand, String document, String demandStatus,
      LocalDate demandDate, LocalDate demandDeadline) {
    this.idUser = idUser;
    this.demand = demand;
    this.document = document;
    this.demandStatus = demandStatus;
    this.demandDate = demandDate;
    this.demandDeadline = demandDeadline;
  }

  public DocumentQuery(Integer id, Integer idUser, String demand, String document,
      String demandStatus, LocalDate demandDate, LocalDate demandDeadline) {
    this.id = id;
    this.idUser = idUser;
    this.demand = demand;
    this.document = document;
    this.demandStatus = demandStatus;
    this.demandDate = demandDate;
    this.demandDeadline = demandDeadline;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getIdUser() {
    return idUser;
  }

  public void setIdUser(Integer idUser) {
    this.idUser = idUser;
  }

  public String getDemand() {
    return demand;
  }

  public void setDemand(String demand) {
    this.demand = demand;
  }

  public String getDocument() {
    return document;
  }

  public void setDocument(String document) {
    this.document = document;
  }

  public String getDemandStatus() {
    return demandStatus;
  }

  public void setDemandStatus(String demandStatus) {
    this.demandStatus = demandStatus;
  }

  public LocalDate getDemandDate() {
    return demandDate;
  }

  public void setDemandDate(LocalDate demandDate) {
    this.demandDate = demandDate;
  }

  public LocalDate getDemandDeadline() {
    return demandDeadline;
  }

  public void setDemandDeadline(LocalDate demandDeadline) {
    this.demandDeadline = demandDeadline;
  }

}
