package Entity;

import java.time.LocalDate;

public class UserDocument {

    private Integer id;
    private Integer userId;
    private String documentName;
    private String document;
    private LocalDate date;

    public UserDocument(Integer userId, String documentName, String document, LocalDate date) {
        this.userId = userId;
        this.documentName = documentName;
        this.document = document;
        this.date = date;
    }


    public UserDocument(Integer id, Integer userId, String documentName, String document,
        LocalDate date) {
        this.id = id;
        this.userId = userId;
        this.documentName = documentName;
        this.document = document;
        this.date = date;
    }

    public UserDocument() {
  
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}




