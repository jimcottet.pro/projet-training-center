package Entity;

import java.time.LocalDate;

public class NewsComment {

  private int id;
  private News news;
  private String author;
  private String authorStatus;
  private String content;
  private LocalDate creationDate;

  public NewsComment(News news, String author, String authorStatus, String content,
      LocalDate creationDate) {
    this.news = news;
    this.author = author;
    this.authorStatus = authorStatus;
    this.content = content;
    this.creationDate = creationDate;
  }

  public NewsComment() {
  }

  public NewsComment(int id, String author, String authorStatus, String content,
      LocalDate creationDate) {
    this.id = id;
    this.author = author;
    this.authorStatus = authorStatus;
    this.content = content;
    this.creationDate = creationDate;
  }

  public NewsComment(String author, String authorStatus, String content,
      LocalDate creationDate) {
    this.author = author;
    this.authorStatus = authorStatus;
    this.content = content;
    this.creationDate = creationDate;
  }

  public NewsComment(int id, String author, String authorStatus, String content,
      LocalDate creationDate, News news) {
    this.id = id;
    this.author = author;
    this.authorStatus = authorStatus;
    this.content = content;
    this.creationDate = creationDate;
    this.news = news;
  }



  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public News getNews() {
    return news;
  }

  public void setNews(News news) {
    this.news = news;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getAuthorStatus() {
    return authorStatus;
  }

  public void setAuthorStatus(String authorStatus) {
    this.authorStatus = authorStatus;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public LocalDate getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDate creationDate) {
    this.creationDate = creationDate;
  }
}
