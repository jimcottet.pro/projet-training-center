package Entity;

import java.time.LocalDate;

public class User {
    private Integer id;
    private Integer promotionId;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String password;
    private String address;
    private String status;

  
    public User(Integer promotionId, String firstName, String lastName, String birthDate, String password,
            String address, String status) {
        this.promotionId = promotionId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = LocalDate.parse(birthDate);
        this.password = password;
        this.address = address;
        this.status = status;
    }

    public User(Integer id, Integer promotionId, String firstName, String lastName, String birthDate,
            String password, String address, String status) {
        this.id = id;
        this.promotionId = promotionId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = LocalDate.parse(birthDate);
        this.password = password;
        this.address = address;
        this.status = status;
    }

    public User() {
  
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = LocalDate.parse(birthDate);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
  
}

