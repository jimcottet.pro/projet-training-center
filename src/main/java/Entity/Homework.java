package Entity;

import java.time.LocalDate;

public class Homework {

  private int id;
  private int promotionId;
  private String title;
  private String theme;
  private String author;
  private LocalDate creationDate;
  private String content;
  private LocalDate dateToFinish;
  private String authorStatus;

  public Homework() {

  }
  public Homework(int promotionId, String title, String theme, String author,
      LocalDate creationDate, String content, LocalDate dateToFinish, String authorStatus) {
    this.promotionId = promotionId;
    this.title = title;
    this.theme = theme;
    this.author = author;
    this.creationDate = creationDate;
    this.content = content;
    this.dateToFinish = dateToFinish;
    this.authorStatus = authorStatus;
  }

  public Homework(int id, int promotionId, String title, String theme, String author,
      LocalDate creationDate, String content, LocalDate dateToFinish, String authorStatus) {
    // Truc de Barth
    setId(id);
    this.promotionId = promotionId;
    this.title = title;
    this.theme = theme;
    this.author = author;
    this.creationDate = creationDate;
    this.content = content;
    this.dateToFinish = dateToFinish;
    this.authorStatus = authorStatus;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    if (id > 1) {
      this.id = id;
    } else {
      System.out.println("Ca marche pô...");
    }
  }
  public int getPromotionId() {
    return promotionId;
  }
  public void setPromotionId(int promotionId) {
    this.promotionId = promotionId;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getTheme() {
    return theme;
  }
  public void setTheme(String theme) {
    this.theme = theme;
  }
  public String getAuthor() {
    return author;
  }
  public void setAuthor(String author) {
    this.author = author;
  }
  public LocalDate getCreationDate() {
    return creationDate;
  }
  public void setCreationDate(LocalDate creationDate) {
    this.creationDate = creationDate;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }
  public LocalDate getDateToFinish() {
    return dateToFinish;
  }
  public void setDateToFinish(LocalDate dateToFinish) {
    this.dateToFinish = dateToFinish;
  }
  public String getAuthorStatus() {
    return authorStatus;
  }
  public void setAuthorStatus(String authorStatus) {
    this.authorStatus = authorStatus;
  }

  
}
