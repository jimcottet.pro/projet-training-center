package Entity;

import java.time.LocalDate;
import java.util.List;

public class News {

  private Integer id;
  private String title;
  private String theme;
  private LocalDate datePublishing;
  private String image;
  private String content;
  private LocalDate dateEvent;
  private List<NewsComment> nCList;


  public News() {
  }

  public News(Integer id, String title, String theme, LocalDate datePublishing,
      String image, String content, LocalDate dateEvent) {
    this.id = id;
    this.title = title;
    this.theme = theme;
    this.datePublishing = datePublishing;
    this.image = image;
    this.content = content;
    this.dateEvent = dateEvent;
  }

  public News(String title, String theme, LocalDate datePublishing, String image,
      String content, LocalDate dateEvent) {
    this.title = title;
    this.theme = theme;
    this.datePublishing = datePublishing;
    this.image = image;
    this.content = content;
    this.dateEvent = dateEvent;
  }

  public News(Integer id, String title, String theme, LocalDate datePublishing,
      String image, String content, LocalDate dateEvent, List<NewsComment> nCList) {
    this.id = id;
    this.title = title;
    this.theme = theme;
    this.datePublishing = datePublishing;
    this.image = image;
    this.content = content;
    this.dateEvent = dateEvent;
    this.nCList = nCList;
  }

  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getTheme() {
    return theme;
  }
  public void setTheme(String theme) {
    this.theme = theme;
  }
  public LocalDate getDatePublishing() {
    return datePublishing;
  }
  public void setDatePublishing(LocalDate datePublishing) {
    this.datePublishing = datePublishing;
  }
  public String getImage() {
    return image;
  }
  public void setImage(String image) {
    this.image = image;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }
  public LocalDate getDateEvent() {
    return dateEvent;
  }
  public void setDateEvent(LocalDate dateEvent) {
    this.dateEvent = dateEvent;
  }
  public List<NewsComment> getnCList() {
    return nCList;
  }
  public void setnCList(List<NewsComment> nCList) {
    this.nCList = nCList;
  }

  public void displayProps() {
    System.out.println(getTitle());
    System.out.println(getTheme());
    System.out.println(getContent());
    System.out.println(getDatePublishing());
    System.out.println();
      for (NewsComment nc : nCList) {
        System.out.println(nc.getId());
        System.out.println(nc.getAuthor());
        System.out.println(nc.getContent());
        System.out.println(nc.getCreationDate() + "\n");
      }
  }
}
