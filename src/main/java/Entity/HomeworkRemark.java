package Entity;

import java.time.LocalDate;

public class HomeworkRemark {

  private int id;
  private int homeworkId;
  private String author;
  private String authorStatus;
  private String content;
  private LocalDate creationDate;

  public HomeworkRemark() {

  }
  public HomeworkRemark(int homeworkId, String author, String authorStatus, String content,
      LocalDate creationDate) {
    this.homeworkId = homeworkId;
    this.author = author;
    this.authorStatus = authorStatus;
    this.content = content;
    this.creationDate = creationDate;
  }
  public HomeworkRemark(int id, int homeworkId, String author, String authorStatus, String content,
      LocalDate creationDate) {
    this.id = id;
    this.homeworkId = homeworkId;
    this.author = author;
    this.authorStatus = authorStatus;
    this.content = content;
    this.creationDate = creationDate;
  }

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public int getHomeworkId() {
    return homeworkId;
  }
  public void setHomeworkId(int homeworkId) {
    this.homeworkId = homeworkId;
  }
  public String getAuthor() {
    return author;
  }
  public void setAuthor(String author) {
    this.author = author;
  }
  public String getAuthorStatus() {
    return authorStatus;
  }
  public void setAuthorStatus(String authorStatus) {
    this.authorStatus = authorStatus;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }
  public LocalDate getCreationDate() {
    return creationDate;
  }
  public void setCreationDate(LocalDate creationDate) {
    this.creationDate = creationDate;
  }

}
