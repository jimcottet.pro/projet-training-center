package Entity;

public class Promotion {
    private int id;
    private String name;

    public Promotion() {
    }
    public Promotion(String name) {
        this.name = name;
    }
    public Promotion(int id, String name) {
        setId(id);
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {

        if (id>0) {
            this.id = id;
        } else {
            System.out.println("merde");
        }

    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

  
}
