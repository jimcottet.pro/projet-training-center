package Repository;

import Entity.DocumentQueryRemark;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DocumentQueryRemarkRepository {

  DocumentQueryRemark dQR;
  List<DocumentQueryRemark> dQRList = new ArrayList<>();
  Connection connect;

  //CREATE
  public boolean save(DocumentQueryRemark dQR) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "INSERT INTO document_query_remarks (document_query_id, author, author_status,"
              + " content, creation_date) "
              + "VALUES (?, ?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
      stmt.setInt(1, dQR.getDocumentQueryId());
      stmt.setString(2, dQR.getAuthor());
      stmt.setString(3, dQR.getAuthorStatus());
      stmt.setString(4, dQR.getContent());
      stmt.setDate(5, Date.valueOf(dQR.getCreationDate()));
      boolean done = stmt.executeUpdate()== 1;
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        dQR.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
  //READ
  public List<DocumentQueryRemark> findAllByQuery(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM document_query_remarks"
              + " WHERE document_query_remark_id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        dQR = new DocumentQueryRemark(
            rs.getInt("document_query_remark_id"),
            rs.getInt("document_query_id"),
            rs.getString("author"),
            rs.getString("author_status"),
            rs.getString("content"),
            rs.getDate("creation_date").toLocalDate()
        );
        dQRList.add(dQR);
      }
      return dQRList;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  public DocumentQueryRemark findById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM document_query_remarks "
              + "WHERE document_query_id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        dQR = new DocumentQueryRemark(
            rs.getInt("document_query_remark_id"),
            rs.getInt("document_query_id"),
            rs.getString("author"),
            rs.getString("author_status"),
            rs.getString("content"),
            rs.getDate("creation_date").toLocalDate()
        );
      }
      return dQR;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  public boolean deleteById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "DELETE FROM document_query_remarks "
              + "WHERE document_query_remark_id = ?");
          stmt.setInt(1, id);
          return stmt.executeUpdate() == 1;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

}
