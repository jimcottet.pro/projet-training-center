package Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.xdevapi.PreparableStatement;

import Entity.News;

public class NewsRepository {

    List<News> newsList = new ArrayList<News>();
    News news;
    Connection connect;
    NewsCommentRepository newsCommentRepository = new NewsCommentRepository();

    // Create
    public void save (News news)  {
        try{
            connect = Connector.getConnection();
            PreparedStatement stmt =  connect.prepareStatement("INSERT INTO news "
                + "(title, theme, date_publishing, image, content, date_event)"
                + "VALUES (?, ?, ?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
                stmt.setString(1, news.getTitle());
                stmt.setString(2, news.getTheme());
                stmt.setDate(3, Date.valueOf(news.getDatePublishing()));
                stmt.setString(4, news.getImage());
                stmt.setString(5, news.getContent());
                stmt.setDate(6, Date.valueOf(news.getDateEvent()));
                stmt.executeUpdate();
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                  news.setId(rs.getInt(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
              }
            }
// READ
  public News findById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM news WHERE news_id = ?"
      );
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        news = new News(
            rs.getInt("news_id"),
            rs.getString("title"),
            rs.getString("theme"),
            rs.getDate("date_publishing").toLocalDate(),
            rs.getString("image"),
            rs.getString("content"),
            rs.getDate("date_event").toLocalDate(),
            newsCommentRepository.findAllByNewsId(rs.getInt("news_id"))
        );
        return news;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return news;
  }

// DELETE
 public void deleteById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "DELETE FROM news "
          + "WHERE news_id = ?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  } 
}   