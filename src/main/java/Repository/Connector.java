package Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Connector {

  public static Connection getConnection() throws SQLException {
    return DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/jim_jas_hen_pjct");
  }

  static public Integer getInteger(ResultSet rs, String strColName) throws SQLException {
    int nValue = rs.getInt(strColName);
    return rs.wasNull() ? null : nValue;
  }

}
