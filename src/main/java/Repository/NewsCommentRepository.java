package Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entity.News;
import Entity.NewsComment;

public class NewsCommentRepository {

    List<NewsComment> nCList = new ArrayList<NewsComment>();
    NewsComment newsComment;
    Connection connect;


    // Create
    public void save (NewsComment newsComment){
        try{
            connect = Connector.getConnection();
            PreparedStatement stmt = connect.prepareStatement("INSERT INTO news_comments"
            + "(news_id, author, author_status, content, creation_date)"
            + "VALUES (?, ?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, newsComment.getNews().getId());
            stmt.setString(2, newsComment.getAuthor());
            stmt.setString(3, newsComment.getAuthorStatus());
            stmt.setString(4, newsComment.getContent());
            stmt.setDate(5, Date.valueOf(newsComment.getCreationDate()));
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
              newsComment.setId(rs.getInt(1));
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    // READ
  public NewsComment findById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM news_comments WHERE news_comment_id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        newsComment = new NewsComment(
            rs.getInt("news_comment_id"),
            rs.getString("author"),
            rs.getString("author_status"),
            rs.getString("content"),
            rs.getDate("creation_date").toLocalDate()
        );
        return newsComment;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<NewsComment> findAllByNewsId(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM news_comments WHERE news_id = ?"
      );
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        newsComment = new NewsComment(
            rs.getInt("news_comment_id"),
            rs.getString("author"),
            rs.getString("author_status"),
            rs.getString("content"),
            rs.getDate("creation_date").toLocalDate()
        );
        nCList.add(newsComment);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return nCList;
  }
    // DELETE
    public void deleteById(int id) {
        try {
          connect = Connector.getConnection();
          PreparedStatement stmt = connect.prepareStatement(
              "DELETE FROM news_comments "
              + "WHERE news_comment_id = ?");
          stmt.setInt(1, id);
          stmt.executeUpdate();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

    