package Repository;

import Entity.User;
import Entity.UserDocument;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDocumentRepository {

  List<UserDocument> listUD = new ArrayList<>();
  UserDocument uD;
  Connection connect;

  // CREATE
  public void save(UserDocument uD) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement("INSERT INTO user_documents "
          + "(id_user, document_name, document, date)"
          + "VALUES (?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
      stmt.setInt(1, uD.getUserId());
      stmt.setString(2, uD.getDocumentName());
      stmt.setString(3, uD.getDocument());
      stmt.setDate(4, Date.valueOf(uD.getDate()));
      stmt.executeUpdate();
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        uD.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  //READ
  public UserDocument findByid(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * user_documents WHERE id_user_document = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        uD = new UserDocument(
            rs.getInt("id_user_document"),
            rs.getInt("id_user"),
            rs.getString("document_name"),
            rs.getString("document"),
            rs.getDate("date").toLocalDate()
        );
        return uD;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<UserDocument> findByUserId(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM user_documents WHERE id_user = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        uD = new UserDocument(
            rs.getInt("id_user_document"),
            rs.getInt("id_user"),
            rs.getString("document_name"),
            rs.getString("document"),
            rs.getDate("date").toLocalDate());
        listUD.add(uD);
      }
      return listUD;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  // DELETE
  public boolean deleteById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(""
          + "DELETE FROM user_documents "
          + "WHERE id_user_document = ?");
      stmt.setInt(1, id);
      return  stmt.executeUpdate() == 1;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}

