package Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entity.Homework;

public class HomeworkRepository {

  Homework homework;
  List<Homework> homeworkList = new ArrayList<>();
  Connection connect;

  //Create

  public boolean save(Homework homework) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "INSERT INTO homeworks (promotion_id, title, theme, author, creation_date, content, date_to_finish, author_status)" +
              " VALUES (?, ?, ?, ?, ?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setInt(1, homework.getPromotionId());
      stmt.setString(2, homework.getTitle());
      stmt.setString(3, homework.getTheme());
      stmt.setString(4, homework.getAuthor());
      stmt.setDate(5, Date.valueOf(homework.getCreationDate()));
      stmt.setString(6, homework.getContent());
      stmt.setDate(7, Date.valueOf(homework.getDateToFinish()));
      stmt.setString(8, homework.getAuthorStatus());
      boolean done = stmt.executeUpdate() == 1;
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        homework.setId(rs.getInt(1));
      }
      return done;
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return false;

  }

  public Homework findById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM homeworks WHERE homework_id = ?"
      );
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        homework = new Homework(
            rs.getInt("homework_id"),
            rs.getInt("promotion_id"),
            rs.getString("title"),
            rs.getString("theme"),
            rs.getString("author"),
            rs.getDate("creation_date").toLocalDate(),
            rs.getString("content"),
            rs.getDate("date_to_finish").toLocalDate(),
            rs.getString("author_status")
        );
        return homework;
      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  public List<Homework> findAllByPromotionId(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM homeworks"
              + " WHERE promotion_id = ? ");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        homework = new Homework(
            rs.getInt("homework_id"),
            rs.getInt("promotion_id"),
            rs.getString("title"),
            rs.getString("theme"),
            rs.getString("author"),
            rs.getDate("creation_date").toLocalDate(),
            rs.getString("content"),
            rs.getDate("date_to_finish").toLocalDate(),
            rs.getString("author_status")
        );
        homeworkList.add(homework);
      }
      return homeworkList;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean deleteById (int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "DELETE FROM homeworks WHERE homework_id = ?"
      );
      stmt.setInt(1, id);
      return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return false;
  }
}
