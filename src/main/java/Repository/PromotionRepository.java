package Repository;

import Entity.Promotion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PromotionRepository {

  Promotion promotion;
  List<Promotion> promotioList = new ArrayList<>();
  Connection connect;

  //CREATE
  public void save(Promotion promotion) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "INSERT INTO promotion (promotion_name) "
              + "VALUE ?", PreparedStatement.RETURN_GENERATED_KEYS);
      stmt.setString(1, promotion.getName());
      stmt.executeUpdate();
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        promotion.setId(1);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
  //READ
  public List<Promotion> findAll() {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement("SELECT * FROM promotion");
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        promotion = new Promotion(
            rs.getInt("promotion_id"),
            rs.getString("promotion_name")
        );
        promotioList.add(promotion);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return promotioList;
  }

  public Promotion findById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM promotion WHERE promotion_id = ? ");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        promotion = new Promotion(
            rs.getInt("promotion_id"),
            rs.getString("promotion_name")
        );
        return promotion;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  //UPDATE
  public void update(Promotion promotion) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "UPDATE promotion SET promotion_name = ? WHERE promotion_id = ?");
      stmt.setString(1, promotion.getName());
      stmt.setInt(2, promotion.getId());
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
  //DELETE
  public void deleteById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "DELETE promotion "
              + "WHERE promotion_id = ?");
      stmt.setInt(1, id);
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }



}
