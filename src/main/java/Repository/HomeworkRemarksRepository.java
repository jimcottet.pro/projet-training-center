package Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.xdevapi.PreparableStatement;

import Entity.HomeworkRemark;

public class HomeworkRemarksRepository {

    List<HomeworkRemark> homeworkRList = new ArrayList<HomeworkRemark>();
    HomeworkRemark homeworkRemark;
    Connection connect;

   // Read
   public  void save (HomeworkRemark homeworkRemark){
       try{
           connect = Connector.getConnection();
           PreparedStatement stmt = connect.prepareStatement("INSERT INTO homework_remarks"
           + "(homework_id, author, author_status, content, creation_date)"
           + " VALUES (?, ?, ?, ?, ?) ", PreparedStatement.RETURN_GENERATED_KEYS);
           stmt.setInt(1, homeworkRemark.getHomeworkId());
           stmt.setString(2,homeworkRemark.getAuthor());
           stmt.setString(3, homeworkRemark.getAuthorStatus());
           stmt.setString(4, homeworkRemark.getContent());
           stmt.setDate(5, Date.valueOf(homeworkRemark.getCreationDate()));
           stmt.executeUpdate();
           ResultSet rs = stmt.getGeneratedKeys();
           if (rs.next()) {
             homeworkRemark.setId(rs.getInt(1));
           }
        }catch(SQLException e) {
            e.printStackTrace();
        }
       }
   //Read
   public List<HomeworkRemark> findAllByHomework(int id){
    try{
           connect = Connector.getConnection();
           PreparedStatement stmt = connect.prepareStatement("SELECT * FROM homework_remarks WHERE homework_id = ?");
           stmt.setInt(1, id);
           ResultSet rs = stmt.executeQuery(); 
           while (rs.next()) {
               homeworkRemark = new HomeworkRemark(
                rs.getInt("homework_remarks_id"),
                rs.getInt("homework_id"),
                rs.getString("author"),
                rs.getString("author_status"),
                rs.getString("content"),
                rs.getDate("creation_date").toLocalDate()); 
                homeworkRList.add(homeworkRemark);
            }
          } catch (SQLException e) {
            e.printStackTrace();
          }
          return homeworkRList;
    }

    public HomeworkRemark findById(int id) {
        try {
        connect = Connector.getConnection();
        PreparedStatement stmt = connect.prepareStatement("SELECT * FROM homework_remarks "
            + "WHERE homework_remarks_id = ?");
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
          homeworkRemark = new HomeworkRemark(
              rs.getInt("homework_remarks_id"),
              rs.getInt("homework_id"),
              rs.getString("author"),
              rs.getString("author_status"),
              rs.getString("content"),
              rs.getDate("creation_date").toLocalDate());
        }

        } catch (SQLException e) {
          e.printStackTrace();
        }
        return homeworkRemark;
    }
//Delete 
    public void deleteById(int id){
        try{
          connect = Connector.getConnection();
          PreparedStatement stmt = connect.prepareStatement(""
            + "DELETE FROM homework_remarks"
            + " WHERE homework_id = ?");
          stmt.setInt(1,id);
          stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}    

  