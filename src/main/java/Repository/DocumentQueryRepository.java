package Repository;

import Entity.DocumentQuery;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DocumentQueryRepository {

  List<DocumentQuery> dQList = new ArrayList<>();
  DocumentQuery documentQuery;
  Connection connect;

  //CREATE
  public boolean save(DocumentQuery documentQuery) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "INSERT INTO document_querys (id_user, demand, document, "
              + "demand_status, demand_date, demand_deadline)"
              + "VALUES (?, ?, ?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
      stmt.setInt(1, documentQuery.getIdUser());
      stmt.setString(2, documentQuery.getDemand());
      stmt.setString(3, documentQuery.getDocument());
      stmt.setString(4, documentQuery.getDemandStatus());
      stmt.setDate(5, Date.valueOf(documentQuery.getDemandDate()));
      stmt.setDate(6, Date.valueOf(documentQuery.getDemandDeadline()));
      boolean done = stmt.executeUpdate() == 1;
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        documentQuery.setId(rs.getInt(1));
      }
      return done;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
  //READ
  public List<DocumentQuery> findAllByUser(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT *  FROM document_querys "
              + "WHERE id_user = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        documentQuery = new DocumentQuery(
            rs.getInt("document_querys_id"),
            rs.getInt("id_user"),
            rs.getString("demand"),
            rs.getString("document"),
            rs.getString("demand_status"),
            rs.getDate("demand_date").toLocalDate(),
            rs.getDate("demand_deadline").toLocalDate()
        );
        dQList.add(documentQuery);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return dQList;
  }

  public DocumentQuery findById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "SELECT * FROM document_querys "
              + "WHERE document_querys_id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        documentQuery = new DocumentQuery(
            rs.getInt("document_querys_id"),
            rs.getInt("id_user"),
            rs.getString("demand"),
            rs.getString("document"),
            rs.getString("demand_status"),
            rs.getDate("demand_date").toLocalDate(),
            rs.getDate("demand_deadline").toLocalDate()
        );
        return documentQuery;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  //DELETE
  public boolean deleteById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(
          "DELETE FROM document_querys "
              + "WHERE document_querys_id = ?");
      stmt.setInt(1, id);
      return stmt.executeUpdate() == 1;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

}
