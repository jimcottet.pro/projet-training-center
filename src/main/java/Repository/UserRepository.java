package Repository;

import static java.lang.String.valueOf;

import Entity.User;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

  List<User> userList = new ArrayList<User>();
  User user;
  Connection connect;

  // CREATE
  public boolean save(User user) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt =  connect.prepareStatement("INSERT INTO users "
          + "(promotion_id, first_name, last_name, birth_date, password, address, status)"
          + " VALUES (?, ?, ?, ?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
      stmt.setInt(1, user.getPromotionId());
      stmt.setString(2, user.getFirstName());
      stmt.setString(3, user.getLastName());
      stmt.setDate(4, Date.valueOf(user.getBirthDate()));
      stmt.setString(5, user.getPassword());
      stmt.setString(6, user.getAddress());
      stmt.setString(7, user.getStatus());
      boolean done = stmt.executeUpdate() == 1;
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        user.setId(rs.getInt(1));
      }
      return done;
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
  // READ
  public List<User> findAllByPromo(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement("SELECT * FROM users "
          + "WHERE promotion_id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        user = new User(
            rs.getInt("id_user"),
            Connector.getInteger(rs, "promotion_id"),
            rs.getString("first_name"),
            rs.getString("last_name"),
            rs.getString("birth_date"),
            rs.getString("password"),
            rs.getString("address"),
            rs.getString("status"));
        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return userList;
  }

  public User findById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement("SELECT * FROM users WHERE id_user = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        user = new User(
            rs.getInt("id_user"),
            Connector.getInteger(rs, "promotion_id"),
            rs.getString("first_name"),
            rs.getString("last_name"),
            rs.getString("birth_date"),
            rs.getString("password"),
            rs.getString("address"),
            rs.getString("status"));
        return user;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  // UPDATE
  public boolean updateById(User user) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt =  connect.prepareStatement("UPDATE users "
          + "SET promotion_id = ?,"
          + "first_name = ?,"
          + "last_name = ?, "
          + "birth_date = ?, "
          + "password = ?, "
          + "address = ?, "
          + "status = ? "
          + "WHERE id_user = ?;");

      stmt.setObject(1, user.getPromotionId());
      stmt.setString(2, user.getFirstName());
      stmt.setString(3, user.getLastName());
      stmt.setDate(4, Date.valueOf(user.getBirthDate()));
      stmt.setString(5, user.getPassword());
      stmt.setString(6, user.getAddress());
      stmt.setString(7, user.getStatus());
      stmt.setInt(8, user.getId());

      return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
  // DELETE
  public boolean deleteById(int id) {
    try {
      connect = Connector.getConnection();
      PreparedStatement stmt = connect.prepareStatement(""
          + "DELETE FROM users "
          + "WHERE id_user = ?");
      stmt.setInt(1, id);
       return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }




}
