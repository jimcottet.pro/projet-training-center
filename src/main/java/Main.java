import Entity.DocumentQuery;
import Entity.DocumentQueryRemark;
import Entity.Homework;
import Entity.HomeworkRemark;
import Entity.News;
import Entity.NewsComment;
import Entity.Promotion;
import Entity.User;
import Entity.UserDocument;
import Repository.DocumentQueryRemarkRepository;
import Repository.DocumentQueryRepository;
import Repository.HomeworkRemarksRepository;
import Repository.HomeworkRepository;
import Repository.NewsCommentRepository;
import Repository.NewsRepository;
import Repository.PromotionRepository;
import Repository.UserDocumentRepository;
import Repository.UserRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) {

    // UserRepo

    //List<User> userList = new ArrayList<>();
    //UserRepository uR = new UserRepository();
    //User user = new User(2, "Jim", "Cottet", "1994-08-17",
    //    "1234", "7 rue", "Eleve");

    //System.out.println(uR.save(user));
    //User userTest =  uR.findById(user.getId());
    //userList = uR.findAllByPromo(userTest.getPromotionId());
    //for (User u : userList) {
    //  System.out.println(u.getFirstName());
    //}
    //System.out.println(uR.deleteById(userTest.getId()));

    // UserDocRepo

    //List<UserDocument> userDocList = new ArrayList<>();
    //UserDocumentRepository uDR = new UserDocumentRepository();
    //UserDocument userDoc = new UserDocument(2, "doc", "doc/place",
    //    LocalDate.parse("1994-08-17"));
    //uDR.save(userDoc);
    //userDocList =  uDR.findByUserId(userDoc.getUserId());
    //for (UserDocument uD : userDocList) {
    //  System.out.println(uD.getDocumentName());
    //}
    //uDR.deleteById(userDoc.getId());

    // HomeworkRepo

    //List<Homework> homeworkList = new ArrayList<>();
    //HomeworkRepository hR = new HomeworkRepository();
    //Homework homework = new Homework(2, "dksld", "djkdfj", "dklfm",
    //    LocalDate.parse("2010-11-12"),
    //    "fkdfldk", LocalDate.parse("2010-11-11"), "Formateur");

    //System.out.println(hR.save(homework));
    //Homework homeworkTest = hR.findById(homework.getId());
    //homeworkList =  hR.findAllByPromotionId(homeworkTest.getPromotionId());
    //for (Homework h : homeworkList) {
    //  System.out.println(h.getTitle());
    //}
    //System.out.println(hR.deleteById(homeworkTest.getId()));

    // HomeworkRemarksRepo

    //List<HomeworkRemark> homeworkRemarkList = new ArrayList<>();
    //HomeworkRemarksRepository homeworkRemarksRepository = new HomeworkRemarksRepository();
    //HomeworkRemark homeworkRemark = new HomeworkRemark(2, "fg", "fg",
    //    "df", LocalDate.parse("2010-11-11"));

    //homeworkRemarksRepository.save(homeworkRemark);
    //HomeworkRemark homeworkRemarkTest = homeworkRemarksRepository.findById(homeworkRemark.getId());
    //homeworkRemarkList = homeworkRemarksRepository.findAllByHomework(homeworkRemark.getHomeworkId());
    //for (HomeworkRemark hr : homeworkRemarkList) {
    //  System.out.println(hr.getAuthor());
    //}
    //homeworkRemarksRepository.deleteById(homeworkRemarkTest.getId());

    // NewsRepo et NewsCommentRepo

    //NewsRepository newsRepository = new NewsRepository();
    //NewsCommentRepository newsCommentRepository = new NewsCommentRepository();
    //News news = new News("Hola", "Hallo", LocalDate.parse("2010-11-11"),
    //    "img/loc", "djfldkj", LocalDate.parse("2010-11-11"));
    //newsRepository.save(news);
    //NewsComment newsComment = new NewsComment(news, "ksdl", "sdsdd",
    //    "JKDJD", LocalDate.parse("2010-11-11"));
    //NewsComment newsComment1 = new NewsComment(news, "dkjfk", "sddfdfd",
    //    "djkfhdkjfh fdfd", LocalDate.parse("2018-12-11"));
    //newsCommentRepository.save(newsComment);
    //newsCommentRepository.save(newsComment1);
    //news = newsRepository.findById(news.getId());

    //news.displayProps();

    //newsCommentRepository.deleteById(newsComment.getId());
    //newsCommentRepository.deleteById(newsComment1.getId());
    //newsRepository.deleteById(news.getId());

    // PromotionRepo

    //PromotionRepository promotionRepository = new PromotionRepository();
    //Promotion promotion = new Promotion("Laravel");
    //promotionRepository.save(promotion);
    //Promotion promotion1 = promotionRepository.findById(promotion.getId());
    //System.out.println(promotion1.getName());
    //for (Promotion prom : promotionRepository.findAll()) {
    //  System.out.println(prom.getName());
    //}
    //promotionRepository.deleteById(promotion1.getId());

    // DocumentQueryRepo

    //DocumentQueryRepository documentQueryRepository = new DocumentQueryRepository();
    //DocumentQuery documentQuery = new DocumentQuery(2, "demand", "document",
    //    "demand_status", LocalDate.parse("2022-04-04"), LocalDate.parse("2022-04-04"));
    //documentQueryRepository.save(documentQuery);
    //DocumentQuery documentQuery1 = documentQueryRepository.findById(documentQuery.getId());
    //System.out.println(documentQuery1.getDemand());
    //for (DocumentQuery dq : documentQueryRepository.findAllByUser(2) ) {
    //  System.out.println(dq.getDemand());
    //}
    //documentQueryRepository.deleteById(documentQuery1.getId());

    // DocumentQueryRemarkRepo (Ca marche pas et je sais pas pourquoi)

    DocumentQueryRemarkRepository documentQueryRemarkRepository = new DocumentQueryRemarkRepository();
    DocumentQueryRemark documentQueryRemark = new DocumentQueryRemark(1, "author",
        "author_status", "content", LocalDate.parse("2022-11-11"));
    System.out.println(documentQueryRemarkRepository.save(documentQueryRemark));
    DocumentQueryRemark documentQueryRemark1 = documentQueryRemarkRepository.findById(
        documentQueryRemark.getId()
    );
    System.out.println(documentQueryRemark1.getAuthor());
    for (DocumentQueryRemark dqr : documentQueryRemarkRepository.findAllByQuery(1)) {
      System.out.println(dqr.getId());
    }
    documentQueryRemarkRepository.deleteById(documentQueryRemark1.getId());



































    

    







    














    
  }
}