package Repository;

import static org.junit.Assert.*;

import Entity.User;
import org.junit.Test;

public class UserRepositoryTest {

  @Test
  public void save() {
    User user = new User(1,
        "Jim", "Cottet",
        "1995-06-12", "1432", "Rue", "Eleve");
    UserRepository userRepository = new UserRepository();
    assertEquals(true, userRepository.save(user));
  }

  @Test
  public void findAll() {
  }

  @Test
  public void findById() {
  }

  @Test
  public void updateById() {
    User user = new User(1,
        "Jim", "Cottet",
        "1995-06-12", "1432", "Rue", "Eleve");
    UserRepository userRepository = new UserRepository();
    userRepository.save(user);
    assertEquals(true, userRepository.updateById(user));
  }

  @Test
  public void deleteById() {
    User user = new User(1,
        "Jim", "Cottet",
        "1995-06-12", "1432", "Rue", "Eleve");
    UserRepository userRepository = new UserRepository();
    userRepository.save(user);
    assertEquals(true, userRepository.deleteById(user.getId()));
  }
}