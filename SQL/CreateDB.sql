DROP DATABASE IF EXISTS jim_jas_hen_pjct ;

CREATE DATABASE jim_jas_hen_pjct;

USE jim_jas_hen_pjct;

CREATE TABLE promotion (
  promotion_id INT PRIMARY KEY AUTO_INCREMENT,
  promotion_name VARCHAR(64)
);

CREATE TABLE users (
  id_user INT AUTO_INCREMENT PRIMARY KEY,
  promotion_id INT,
  CONSTRAINT fk_user_promotion
  FOREIGN KEY (promotion_id) 
  REFERENCES promotion(promotion_id),
  first_name VARCHAR(64),
  last_name VARCHAR(64),
  birth_date DATE,
  password VARCHAR(64),
  address VARCHAR(128),
  status VARCHAR(32)
);
CREATE TABLE user_documents (
    id_user_document INT AUTO_INCREMENT PRIMARY KEY,
    id_user INT,
    CONSTRAINT fk_userdocuments_user
    FOREIGN KEY (id_user) 
    REFERENCES users(id_user),
    document_name VARCHAR(64),
    document VARCHAR(400),
    date DATE
);
CREATE TABLE document_querys (
  document_querys_id INT PRIMARY KEY AUTO_INCREMENT,
  id_user INT,
  CONSTRAINT fk_docquery_user 
  FOREIGN KEY (id_user) 
  REFERENCES users(id_user),
  demand TEXT,
  document VARCHAR(500),
  demand_status VARCHAR(32),
  demand_date DATE,
  demand_deadline DATE
);

CREATE TABLE document_query_remarks (
  document_query_remark_id INT PRIMARY KEY AUTO_INCREMENT,
  document_query_id INT,
  CONSTRAINT fk_docqueryrmk_docquery
  FOREIGN KEY (document_query_id) 
  REFERENCES document_querys(document_querys_id),
  author VARCHAR(64),
  author_status VARCHAR(32),
  content TEXT,
  creation_date DATE
);
CREATE TABLE news (
    news_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(64),
    theme VARCHAR(64),
    date_publishing DATE,
    image VARCHAR(500),
    content TEXT,
    date_event DATE
);

CREATE TABLE news_comments(
    news_comment_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    news_id INT,
    CONSTRAINT fk_newscom_news 
    FOREIGN KEY (news_id) 
    REFERENCES news(news_id)
    CASCADE,
    author VARCHAR(64),
    author_status VARCHAR(64),
    content TEXT,
    creation_date DATE
);

CREATE TABLE homeworks(
    homework_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    promotion_id INT,
    CONSTRAINT fk_homeworks_promo 
    FOREIGN KEY(promotion_id) 
    REFERENCES promotion(promotion_id),
    title VARCHAR(64),
    theme VARCHAR(64),
    author VARCHAR(64),
    creation_date DATE,
    content TEXT,
    date_to_finish DATE,
    author_status VARCHAR(32)
    
);
CREATE TABLE homework_remarks(
    homework_remarks_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    homework_id INT,
    CONSTRAINT fk_homeworkrmk_homework 
    FOREIGN KEY (homework_id) 
    REFERENCES homeworks(homework_id),
    author VARCHAR(64),
    author_status VARCHAR(32),
    content TEXT,
    creation_date DATE
);

INSERT INTO promotion (promotion_name)
VALUES ("Java"), 
("Python"), 
("JavaScript"), 
("C++"),
("Fortan");

INSERT INTO users(
  promotion_id, first_name, 
  last_name, birth_date, password, address, status)
  VALUES (1, "Jamie", "Rivera", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (2, "Gloria", "Jensen", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (1, "Jason", "Rivera", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (3, "Lee", "Douglas", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (4, "Lee", "Nelson", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (3, "Jesse", "Kameron", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (2, "Anna", "Clark", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (4, "Lee", "Douglas", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (5, "Danielle", "Griffin", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (null, "Robin", "Douglas", "1978-08-13", "1234", "Rue de la vie", "Admin" ),
  (3, "Lee", "Douglas", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (null, "Scott", "Shepard", "1978-08-13", "1234", "Rue de la vie", "Formateur" ),
  (1, "Clark", "Anna", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (2, "Andrew", "Simmons", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (1, "Stacey", "Berry", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (null, "Lee", "Douglas", "1978-08-13", "1234", "Rue de la vie", "Admin" ),
  (5, "Robin", "Douglas", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (null, "Scott", "Shepard", "1978-08-13", "1234", "Rue de la vie", "Formateur" ),
  (4, "Jamie", "Rivera", "1978-08-13", "1234", "Rue de la vie", "Eleve" ),
  (null, "Gloria", "Jensen", "1978-08-13", "1234", "Rue de la vie", "Formateur" ),
  (null, "Jason", "Robin", "1978-08-13", "1234", "Rue de la vie", "Secrétariat" );

INSERT INTO user_documents (id_user, document_name, document, date)
VALUES (5, "Convention de stage", "./usersdoc/thisuser/doc", "2021-11-09"),
(5, "Convention de stage", "./usersdoc/thisuser/doc", "2021-12-05"),
(3, "Paiement", "./usersdoc/thisuser/doc", "2021-07-13"),
(4, "Convention de stage", "./usersdoc/thisuser/doc", "2021-07-14"),
(1, "Convention de stage", "./usersdoc/thisuser/doc", "2021-07-13"),
(3, "Arrêt Maladie", "./usersdoc/thisuser/doc", "2021-07-13"),
(2, "Paiement", "./usersdoc/thisuser/doc", "2021-07-17"),
(2, "Convention de stage", "./usersdoc/thisuser/doc", "2021-07-13"),
(4, "Arrêt Maladie", "./usersdoc/thisuser/doc", "2021-07-13"),
(2, "Convention de stage", "./usersdoc/thisuser/doc", "2021-08-01"),
(1, "Arrêt Maladie", "./usersdoc/thisuser/doc", "2021-09-13"),
(5, "Paiement", "./usersdoc/thisuser/doc", "2021-07-13");

INSERT INTO document_querys (id_user, demand, document, demand_status, demand_date, demand_deadline)
VALUES (1, "document s\'il vous plait !", "", "En cours", "2021-06-21", "2021-06-26"),
       (8, "document s\'il vous plait !", "./usersdoc/thisuser/do", "Envoyée", "2021-06-21", "2021-06-26"),
       (10, "document s\'il vous plait !", "", "En cours", "2021-06-21", "2021-06-26");

INSERT INTO document_query_remarks(document_query_id, author, author_status, content, creation_date)
VALUES (1, "Jason", "Secrétariat", "Pas encore envoyé !", "2021-06-26" ),
        (1, "Gloria", "Eleve", "Pas encore envoyé !", "2021-06-26" ),
       (3, "Jason", "Secrétariat", "Pas encore envoyé !", "2021-11-26" );

INSERT INTO news (title,theme,date_publishing,image,content,date_event) 
VALUES
    ("Le numérique",
     "Une grande diversité de métiers",
     "2022-02-04",
     "image",
     "Alors que les projets de recrutements sur les métiers numériques foisonnent,
      60 à 80% d’entre eux peinent à embaucher, selon une étude de la Grande école du numérique (GEN).
      Parmi les métiers les plus porteurs figurent les développeurs, les chefs de projet web, 
      les digital business developpers et les data scientists.",
     "2022-04-02"),

    (" Développeur web",
     "Comment devenir Développeur web ?",
     "2021-04-02",
     "image",
     "Chargé de la réalisation d'un site internet et des fonctionnalités qui vont avec,
      le développeur web est un programmateur multi-tâches et multi-languages, 
      très flexible (agile), afin de s'adapter aux nombreux besoins d'un site internet.",
     "2020-01-01"),

    ("Les métiers du design",
     "Définition du design",
     "2020-12-11",
     "image",
     "Les métiers du design ont pour finalité d’améliorer la qualité et la compétitivité
      d’un produit ou d’une gamme de produits, d’un service, d’un espace, d’une communication,
      en intervenant sur les aspects fonctionnels, techniques, esthétiques et sémantiques. ",
     "2019-04-04");

INSERT INTO news_comments (news_id,author,author_status,content,creation_date) 
VALUES
    (1,"CABRAL Pedro",
     "Élève",
     "Le master apporte également un réseau et m’a permis de rencontrer 
      une trentaine de personnes aux profils hétérogènes travaillant dans mon secteur",
     "2021-01-02"),

    (1,"SILVA Jose",
    "Élève",
    "Le design est un processus de réponse à de problématiques stratégiques qui créé de l’innovation, 
     contribue au succès des entreprises et offre une meilleure qualité de vie à travers des produits, 
     des systèmes, des services et des expériences innovantes.",
    "1880-08-08"),

    (3,"SOUZA Caio",
     "Élève",
     "En tant qu'esthéticien de l'objet le designer-produit travaille pour une multitude de secteurs industriels
      ou même artisanaux. Il conçoit le look des objets ou des produits fabriqués en série mais aussi celui de leurs emballages.",
     "1990-09-09");

INSERT INTO homeworks(promotion_id, title, theme, author,creation_date, content, date_to_finish, author_status)
VALUES
(1,"Project SQL",
  "Training Center (Centre de formation)",
  "FREITAS Lucas",
  "2022-10-11",
  "Identifier et lister sous forme de user stories ou de diagramme de use case les fonctionnalités de l'application,
   Créer les maquettes fonctionnelles de quelques écrans de l'application,
   Identifier et schématiser sous forme de diagramme de classe les entités qui persisteront en base de données
   pour que ces fonctionnalités soient possibles",
  "2022-11-11",
  "Formateur"),
(2,"Projet Jeu",
  "Réaliser un petit jeu via de la POO",
  "BARBOSA Felippe",
  "2022-09-11",
  "Initialiser un projet Java maven et le lier à votre gitlab
   Choisir un jeu (exemple de jeu plus bas pour les gens qui n'ont pas d'idée)
   Créer un fichier Readme.md à la racine de votre projet, indiquer le jeu que vous avez choisi,
   et les règles que vous allez coder. Attention, ce fichier est au format Markdown !",
  "2022-10-11" ,
  "Formateur"),
(3,"Gestion d'un club de tennis",
  "Créer une base de données",
  "PEREIRA Joaquin",
  "2022-09-11",
  "Un club de tennis est représenté par un identifiant auto-incrémenté,
   un nom, une adresse (varchar), un code postal (sur 5 chiffres), 
   Insérez au moins 3 clubs dans la base, dont 2 à Lyon.",
  "2022-10-11",
  "Formateur");

INSERT INTO homework_remarks(homework_id,author, author_status,content, creation_date)
VALUES

 (1,"SILVEIRA Anne",
 "Eleve",
 "Désolé, je n’ai pas le temps",
 "2022-01-01"),

 (1,"ZOMMER Luis",
  "Eleve",
  "J’ai trouvé très difficile à faire ",
  "2022-02-02"),

 (2,"ROSA Maria",
  "Eleve",
  "Je n’ai pas le temps de faire",
  "2022-10-10"),
 (3,"MARTINELLI Jose",
  "Eleve",
  "Réussir et je suis très heureux avec le résultat",
  "2022-11-11");



  